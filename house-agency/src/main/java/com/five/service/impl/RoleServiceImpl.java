package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.common.TreeNode;
import com.five.domain.SysPermission;
import com.five.domain.SysRole;
import com.five.mapper.PermissionMapper;
import com.five.mapper.RoleMapper;
import com.five.service.RoleService;
import com.five.vo.RoleVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    //查询所有角色
    @Override
    public DataGridView queryAllRole(RoleVo roleVo) {
        Page<Object> page = PageHelper.startPage(roleVo.getPage(), roleVo.getLimit());
        List<SysRole> data = roleMapper.queryAllRole(roleVo);
        return new DataGridView(page.getTotal(), data);
    }

    //添加角色
    @Override
    @Transactional
    public void addRole(RoleVo roleVo) {
        roleMapper.addRole(roleVo);
    }

    //修改角色
    @Override
    @Transactional
    public void updateRole(RoleVo roleVo) {
        roleMapper.updateRole(roleVo);
    }

    //根据id删除角色
    @Override
    @Transactional
    public void deleteRole(Integer id) {
        // 删除角色表的数据
        roleMapper.deleteRole(id);
        // 根据角色id删除sys_role_permission里面的数据
        roleMapper.deleteRolePermissionByRid(id);
        // 根据角色id删除sys_role_user里面的数据
        roleMapper.deleteRoleUserByRid(id);
    }

    //根据id加载角色管理分配菜单
    @Override
    public DataGridView initRolePermissionTreeJson(Integer id) {
        // 查询所有的可用的菜单
        SysPermission permission = new SysPermission();
        permission.setAvailable(1);
        List<SysPermission> allPermission = permissionMapper.queryAllPermission(permission);
        // 根据角色ID查询当前角色拥有的菜单
        List<SysPermission> rolePermission = permissionMapper.queryPermissionByRoleId(1, id);
        List<TreeNode> data = new ArrayList<>();
        for (SysPermission p1 : allPermission) {
            String checkArr = 0+"";
            for (SysPermission p2 : rolePermission) {
                if (p1.getId() == p2.getId()) {
                    checkArr = 1+"";
                    break;
                }
            }
            data.add(new TreeNode(p1.getId(), p1.getPid(), p1.getTitle(), p1.getOpen() == 1 ? true : false, checkArr));
        }
        return new DataGridView(data);
    }

    //保存角色和菜单的关系
    @Override
    @Transactional
    public void saveRolePermission(RoleVo roleVo) {
        Integer rid=roleVo.getId();
        Integer [] pids=roleVo.getIds();
        //根据rid删除sys_role_menu里面所有数据
        roleMapper.deleteRolePermissionByRid(rid);
        //保存角色和菜单的关系
        for (Integer pid : pids) {
            roleMapper.addRolePermission(rid,pid);
        }
    }

    //根据角色名名查询角色
    public SysRole queryRoleByName(String name){
        return roleMapper.queryRoleByName(name);
    }
}
