package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.common.UUIDUtil;
import com.five.common.UserUtil;
import com.five.common.file.AppFileUtils;
import com.five.common.file.SysConstast;
import com.five.domain.House;
import com.five.mapper.EntrustMapper;
import com.five.mapper.HouseMapper;
import com.five.service.HouseService;
import com.five.vo.EntrustVo;
import com.five.vo.HouseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class HouseServiceImpl implements HouseService {

    @Autowired
    HouseMapper houseMapper;
    @Autowired
    EntrustMapper entrustMapper;

    //后端页面====================================================


    //查询房屋
    @Override
    public List<House> queryAllHouse(HouseVo houseVo){
        List<House> data = houseMapper.queryAllHouse(houseVo);
        return data;
    }

    //添加房屋
    @Override
    public DataGridView addHouse(HouseVo houseVo){
        EntrustVo entrustVo = new EntrustVo();
        entrustVo.setId(houseVo.getId());
        Date date = new Date();
        houseVo.setId(UUIDUtil.getUUID());
        houseVo.setaId(UserUtil.getUser().getId());
        if(houseVo.getType() == 1){
            houseVo.setSaId("00000000000000000000000000000000");
        }
        houseVo.setTime(date);
        houseVo.setStatus(1);
        String filePath = AppFileUtils.updateFileName(houseVo.getImg(), SysConstast.FILE_UPLOAD_TEMP);
        houseVo.setImg(filePath);
        houseMapper.alter();
        int row = houseMapper.addHouse(houseVo);
        if(row == 0){
            return DataGridView.ADD_ERROR;
        }else {
            if(houseVo.getType() != 1){
                entrustMapper.deleteEntrust(entrustVo);
            }
            return DataGridView.ADD_SUCCESS;
        }
    }

    //修改房屋
    @Override
    public DataGridView updateHouse(HouseVo houseVo){
        String filePath = AppFileUtils.updateFileName(houseVo.getImg(), SysConstast.FILE_UPLOAD_TEMP);
        houseVo.setImg(filePath);
        int row = houseMapper.updateHouse(houseVo);
        if(row == 1){
            return DataGridView.UPDATE_SUCCESS;
        }else {
            return DataGridView.UPDATE_ERROR;
        }
    }

    //取消房屋
    @Override
    public DataGridView cancelHouse(HouseVo houseVo){
        int row = houseMapper.cancelHouse(houseVo);
        if(row == 1){
            return DataGridView.CANCEL_SUCCESS;
        }else{
            return DataGridView.CANCEL_ERROR;
        }
    }

    //房屋数量
    @Override
    public DataGridView queryHouseNumber(House house){
        Long data = houseMapper.queryHouseNumber(house.getStatus());
        return new DataGridView(data,"");
    }

    //查询房屋参数(App与后端共用)
    @Override
    public House queryHouseById(HouseVo houseVo){
        House data = houseMapper.queryHouseById(houseVo);
        return data;
    }


    //小程序====================================================


    //查询房屋(App)
    @Override
    public List<House> queryHouse(HouseVo houseVo){
        houseVo.setStatus(3);
        List<House> data = houseMapper.queryHouse(houseVo);
        return data;
    }


    //最新房屋(App)
    @Override
    public List<House> queryHouseByNew(HouseVo houseVo){
        List<House> data = houseMapper.queryHouseByNew(houseVo);
        return data;
    }

}
