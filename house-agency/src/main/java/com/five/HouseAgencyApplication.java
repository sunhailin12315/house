package com.five;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HouseAgencyApplication {

    public static void main(String[] args) {
        SpringApplication.run(HouseAgencyApplication.class, args);
    }

}
