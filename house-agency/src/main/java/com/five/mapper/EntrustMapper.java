package com.five.mapper;

import com.five.domain.Entrust;
import com.five.vo.EntrustVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EntrustMapper {

    //查询委托信息
    List<Entrust> queryEntrust(Entrust entrust);

    //删除委托信息
    int deleteEntrust(Entrust entrust);

    //添加委托信息
    void addEntrust(EntrustVo entrustVo);

    //查询所有委托信息
    List<Entrust> queryAllEntrust(Entrust entrust);

    //委托数量
    Long queryEntrustNumber();
}
