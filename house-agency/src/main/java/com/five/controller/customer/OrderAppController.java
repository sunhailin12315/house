package com.five.controller.customer;


import com.five.common.DataGridView;
import com.five.common.exception.MyException;
import com.five.service.OrderService;
import com.five.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/user/order")
public class OrderAppController {

    @Autowired
    private OrderService orderService;

    //取消订单
    @RequestMapping("/cancelOrder")
    public DataGridView cancelOrder(OrderVo orderVo){
        try {
            orderService.cancelOrder(orderVo);
            return DataGridView.CANCEL_SUCCESS;
        }catch (Exception e){
            return DataGridView.CANCEL_ERROR;
        }
    }

    //查询订单
    @RequestMapping("/lookOrder")
    public DataGridView lookOrder(OrderVo orderVo){
        return orderService.lookOrder(orderVo);
    }

    //购买房屋
    @RequestMapping("/addOrder")
    public DataGridView addOrder(OrderVo orderVo){
        try {
            String oid = orderService.addOrder(orderVo);
            return new DataGridView(200,"购买成功", oid);
        }catch (MyException e){
            return DataGridView.BUY_ERROR2;
        }
        catch (Exception e){
            return DataGridView.BUY_ERROR;
        }
    }

    //支付订单
    @RequestMapping("/payOrder")
    public DataGridView payOrder(String id,Integer inputnum){
        try {
            orderService.payOrder(id,inputnum);
            return DataGridView.PAY_SUCCESS;
        }catch (Exception e){
            e.printStackTrace();
            return DataGridView.PAY_ERROR;
        }
    }

    //支出情况
    @RequestMapping("/allOrderByBid")
    public DataGridView allOrderByBid(OrderVo orderVo){
        return new DataGridView(200,"",orderService.queryAllOrderByBid(orderVo));
    }

    //收入情况
    @RequestMapping("/allOrderBySaid")
    public DataGridView allOrderBySaid(OrderVo orderVo){
        return new DataGridView(200,"",orderService.queryAllOrderBySaid(orderVo));
    }
}
