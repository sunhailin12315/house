package com.five.controller.customer;

import com.five.common.DataGridView;
import com.five.common.UserUtil;
import com.five.common.exception.MyException;
import com.five.config.security.User;
import com.five.domain.SysUser;
import com.five.service.UserService;
import com.five.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/customer")
public class CustomerController {
    @Autowired
    private UserService userService;

    //注册
    @RequestMapping("/register")
    public DataGridView register(UserVo userVo){
        try {
            if(userVo.getUsername().length()<6||userVo.getPassword().length()<6) return DataGridView.REGISTER_ERROR2;
            if(userVo.getPhone().length()<11||userVo.getIdentity().length()<18) return DataGridView.REGISTER_ERROR3;
            userService.addCustomer(userVo);
            return DataGridView.REGISTER_SUCCESS;
        }
        catch (MyException me){
            return DataGridView.ADD_ERROR2;
        }
        catch (Exception e){
            return DataGridView.REGISTER_ERROR;
        }
    }
    //小程序用户登录状态是否存活
    @RequestMapping("/survival")
    public DataGridView survival(){
        try {
            User user = UserUtil.getUser();
            return new DataGridView(200,"用户存在");
        } catch (Exception e){
            return new DataGridView(500,"用户不存在");
        }
    }

    //获取用户信息
    @RequestMapping("/userinfo")
    public DataGridView userinfo(){
        try {
            User user = UserUtil.getUser();
            SysUser sysUser = new SysUser();
            sysUser.setUsername(user.getUsername());
            sysUser.setName(user.getName());
            sysUser.setSex(user.getSex());
            sysUser.setPhone(user.getPhone());
            sysUser.setIdentity(user.getIdentity());
            return new DataGridView(200,"用户存在",sysUser);
        } catch (Exception e){
            return new DataGridView(500,"用户不存在");
        }
    }

    //用户信息修改
    @RequestMapping("/update")
    public DataGridView update(UserVo userVo){
        try {
            if(userVo.getPhone().length()<11||userVo.getIdentity().length()<18) return DataGridView.REGISTER_ERROR3;
            userService.updateInfo(userVo);
            return DataGridView.UPDATE_SUCCESS;
        }
        catch (Exception e){
            e.printStackTrace();
            return DataGridView.UPDATE_ERROR;
        }
    }

    //修改密码
    @RequestMapping("/updatePwd")
    public DataGridView updatePwd(String oldpassword, String newpassword){
        try {
            if(newpassword.length()<6) return DataGridView.UPDATE_PWD_ERROR2;
            userService.updatePwd(oldpassword,newpassword);
            return DataGridView.UPDATE_SUCCESS;
        }
        catch (MyException ue){
            return DataGridView.UPDATE_PWD_ERROR;
        }
        catch (Exception e){
            return DataGridView.UPDATE_ERROR;
        }
    }
}
