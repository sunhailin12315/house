import {request} from "../../request/index.js";

Page({
  data: {
    isChecked:"checked"
  },

  onLoad: function (options) {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.reLaunch({
        url: '../login/index',
      });
      return;
    }
  },
  //出租内容提交
  async formSubmit(e) {
    const rentout = e.detail.value;
    if(rentout.phone==""||rentout.phone==null||
      rentout.cName==""||rentout.cName==null||
      rentout.local==""||rentout.local==null||
      rentout.type==""||rentout.type==null){
        wx.showToast({
           title: "填写信息不能为空",
           icon: 'error',
           mask: true
         });
      return;
    }
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/entrust/addEntrust", method: "POST", data: rentout,header: header});
    const {code,msg} = res.data;
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(code===200){
      setTimeout(() => {
        // 返回上一个页面
        wx.navigateBack({
          delta: 1
        });
      }, 1000);
    }

  },
  //重置
  formReset () {
    this.setData({
      isChecked:"checked"
    })
  }
})