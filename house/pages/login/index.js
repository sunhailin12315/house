import {request} from "../../request/index.js";

Page({
  data: {
    username_focus:false,
    password_focus:false,
    rememberMe: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  //input获得焦点事件
  inputFocus: function(e){
    if(e.target.id == 'username'){
      this.setData({
        username_focus: true
      });
    }else if(e.target.id == 'password'){
      this.setData({
        password_focus: true
      });
    }
  },
  //input失去焦点事件
  inputBlur: function(e){
    if(e.target.id == 'username'){
      this.setData({
        username_focus: false
      });
    }else if(e.target.id == 'password'){
      this.setData({
        password_focus: false
      });
    }
  },
  //跳转注册
  toRegister: function(e){
    wx.navigateTo({
      url: '../register/index',
    })
  },
  //注册
  async formSubmit(e) {
    
    const t = e.detail.value;
    const user={
      username:t.username,
      password:t.password,
      rememberMe: true
    }
    const res = await request({ url: "/doLogin", method: "POST", data: user,header: { "content-type": "application/x-www-form-urlencoded" } });
    const {status,msg} = res.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(status===200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
      }
      setTimeout(() => {
        wx.switchTab({
          url: '../index/index',
        })
      }, 1000);
    }
  },
})